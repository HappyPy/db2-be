create table KATEGORIE
(
    KATEGORIE_ID NUMBER(8) primary key,
    NAZEV        VARCHAR2(25) CHECK ( NAZEV IN ('NOTEBOOK', 'COMPUTER', 'PHONE', 'TABLET',
                                                'GAME_CONSOLE', 'MONITOR', 'TV', 'AUDIO', 'WIFI', 'MOUSE',
                                                'HEADPHONE', 'KEYBOARD', 'PODLOZKA', 'GAMEPAD',
                                                'EXTERNAL_DISK') ) not null,
    POPIS        VARCHAR2(400)
);
create sequence SEQ_KATEGORIE increment by 1 start with 0 minvalue 0 maxvalue 99999;

create table ZBOZI
(
    ZBOZI_ID        NUMBER(8) primary key,
    NAZEV           VARCHAR2(40)   not null,
    CENA_ZA_KUS     NUMBER(5)      not null,
    POCET_NA_SKLADE NUMBER(5)      not null,
    POCET_CELKEM    NUMBER(5)      not null,
    POPIS           VARCHAR2(2500) not null,
    KRATKY_POPIS    VARCHAR2(900)
);
create sequence SEQ_ZBOZI increment by 1 start with 0 minvalue 0 maxvalue 99999;

create table ZBOZI_PHOTO
(
    ZBOZI_PHOTO_ID NUMBER(8) primary key,
    PHOTO          CLOB      not null,
    ZBOZI_ID       NUMBER(8) not null,
    CONSTRAINT fk_ZBOZI_PHOTO_ZBOZI_ID
        FOREIGN KEY (ZBOZI_ID)
            REFERENCES ZBOZI (ZBOZI_ID)
);
create sequence SEQ_ZBOZI_PHOTO increment by 1 start with 0 minvalue 0 maxvalue 99999;

create table KATEGORIE_ZBOZI
(
    KAT_ZBOZI_ID NUMBER(8) primary key,
    KATEGORIE_ID NUMBER(8) not null,
    ZBOZI_ID     NUMBER(8),
    CONSTRAINT fk_KAT_ZBOZI_KAT_ID
        FOREIGN KEY (KATEGORIE_ID)
            REFERENCES KATEGORIE (KATEGORIE_ID),
    CONSTRAINT fk_KAT_ZBOZI_ZBOZI_ID
        FOREIGN KEY (ZBOZI_ID)
            REFERENCES ZBOZI (ZBOZI_ID)
);
create sequence SEQ_KATEGORIE_ZBOZI increment by 1 start with 0 minvalue 0 maxvalue 99999;

create table ADRESA
(
    ADRESA_ID     NUMBER(8) primary key,
    MESTO         VARCHAR2(20) not null,
    ULICE         VARCHAR2(20) not null,
    CISLO_POPISNE VARCHAR2(10) not null,
    PSC           NUMBER(5)    not null
);
create sequence SEQ_ADRESA increment by 1 start with 0 minvalue 0 maxvalue 99999;

create table UZIVATEL
(
    UZIVATEL_ID NUMBER(8) primary key,
    JMENO       VARCHAR2(25) not null,
    PRIJMENI    VARCHAR2(25) not null,
    ADRESA_ID   NUMBER(8)    not null,
    EMAIL       VARCHAR2(24),
    TELEFON     VARCHAR2(24),
    CONSTRAINT fk_UZIVATEL_ADRESA_ID
        FOREIGN KEY (ADRESA_ID)
            REFERENCES ADRESA (ADRESA_ID)
);
create sequence SEQ_UZIVATEL increment by 1 start with 0 minvalue 0 maxvalue 99999;

create table PRIHLASENI
(
    PRIHLASENI_ID NUMBER(8) primary key,
    HESLO         VARCHAR2(100)                                               not null,
    JMENO         VARCHAR2(25) unique                                         not null,
    TYP           VARCHAR2(25) CHECK ( TYP IN ('UZIVATEL', 'ADMINISTRATOR') ) not null,
    UZIVATEL_ID   NUMBER(8),
    CONSTRAINT fk_PRIHLASENI_UZIVATEL_ID
        FOREIGN KEY (UZIVATEL_ID)
            REFERENCES UZIVATEL (UZIVATEL_ID)
);
create sequence SEQ_PRIHLASENI increment by 1 start with 0 minvalue 0 maxvalue 99999;

create table PLATBA
(
    PLATBA_ID  NUMBER(8) primary key,
    PLATBA_TYP VARCHAR2(25) CHECK ( PLATBA_TYP IN ('ONLINE', 'PREVODEM') ) not null,
    ZAPLACENO  DATE
);
create sequence SEQ_PLATBA increment by 1 start with 0 minvalue 0 maxvalue 99999;

INSERT INTO PLATBA
VALUES (SEQ_PLATBA.nextval, 'ONLINE', SYSDATE);
INSERT INTO PLATBA
VALUES (SEQ_PLATBA.nextval, 'PREVODEM', SYSDATE);

CREATE TABLE DOPRAVA
(
    DOPRAVA_ID  NUMBER(8) PRIMARY KEY,
    DOPRAVA_TYP VARCHAR2(25) CHECK ( DOPRAVA_TYP IN ('PPL', 'CESKA POSTA', 'OSOBNI PREVZETI') ) not null
);

CREATE SEQUENCE SEQ_DOPRAVA INCREMENT BY 1 START WITH 0 MINVALUE 0 MAXVALUE 99999;

INSERT INTO DOPRAVA
VALUES (SEQ_DOPRAVA.nextval, 'PPL');
INSERT INTO DOPRAVA
VALUES (SEQ_DOPRAVA.nextval, 'CESKA POSTA');
INSERT INTO DOPRAVA
VALUES (SEQ_DOPRAVA.nextval, 'OSOBNI PREVZETI');

create table STAV_OBJEDNAVKY
(
    STAV_OBJEDNAVKY_ID NUMBER(8) primary key,
    STAV               VARCHAR2(25) CHECK ( STAV IN ('ZALOZENO', 'ZPRACOVANO', 'EXPEDOVANO', 'PRIPRAVENO K ODBERU',
                                                     'DORUCENO') ) not null
);

create table OBJEDNAVKA
(
    OBJEDNAVKA_ID      NUMBER(8) primary key,
    VYTVORENA          DATE      not null,
    PLATBA_ID          NUMBER(8) not null,
    UZIVATEL_ID        NUMBER(8) not null,
    CENA               NUMBER(8) not null,
    DOPRAVA_ID         NUMBER(8) not null,
    STAV_OBJEDNAVKY_ID NUMBER(8),
    DOD_ADRESA         NUMBER(8),
    CONSTRAINT fk_OBJEDNAVKA_PLATBA_ID
        FOREIGN KEY (PLATBA_ID)
            REFERENCES PLATBA (PLATBA_ID),
    CONSTRAINT fk_OBJEDNAVKA_STAV_ID
        FOREIGN KEY (STAV_OBJEDNAVKY_ID)
            REFERENCES STAV_OBJEDNAVKY (STAV_OBJEDNAVKY_ID),
    CONSTRAINT fk_OBJEDNAVKA_UZIVATEL_ID
        FOREIGN KEY (UZIVATEL_ID)
            REFERENCES UZIVATEL (UZIVATEL_ID),
    CONSTRAINT fk_OBJEDNAVKA_DOPRAVA_ID
        FOREIGN KEY (DOPRAVA_ID)
            REFERENCES DOPRAVA (DOPRAVA_ID),
    CONSTRAINT fk_OBJEDNAVKA_FAKT_ADRESA
        FOREIGN KEY (DOD_ADRESA)
            REFERENCES ADRESA (ADRESA_ID)
);
create sequence SEQ_OBJEDNAVKA increment by 1 start with 0 minvalue 0 maxvalue 99999;

create table OBJEDNAVKA_ZBOZI
(
    OBJ_ZBOZI_ID  NUMBER(8) primary key,
    CENA          NUMBER(10, 2) not null,
    MNOZSTVI      NUMBER(5)     not null,
    OBJEDNAVKA_ID NUMBER(8)     not null,
    ZBOZI_ID      NUMBER(8)     not null,
    CONSTRAINT fk_OBJ_ZBOZI_ZBOZI_ID
        FOREIGN KEY (ZBOZI_ID)
            REFERENCES ZBOZI (ZBOZI_ID),
    CONSTRAINT fk_OBJ_ZBOZI_OBJ_ID
        FOREIGN KEY (OBJEDNAVKA_ID)
            REFERENCES OBJEDNAVKA (OBJEDNAVKA_ID)
);
create sequence SEQ_OBJEDNAVKA_ZBOZI increment by 1 start with 0 minvalue 0 maxvalue 99999;

INSERT INTO PRIHLASENI
VALUES (SEQ_PRIHLASENI.nextval, '$2a$10$JmF8JJlI38tQTRAS9Q5e2Oq2Sf.DQm/P3zPlPynPefR/CY5p6BTge', 'admin',
        'ADMINISTRATOR', null);

-- TRIGGERS

CREATE OR REPLACE TRIGGER INSERT_DATE
    BEFORE INSERT
    ON OBJEDNAVKA
    FOR EACH ROW
BEGIN
    :NEW.VYTVORENA := SYSDATE;
END;
