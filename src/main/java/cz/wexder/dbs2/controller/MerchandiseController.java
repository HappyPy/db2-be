package cz.wexder.dbs2.controller;

import cz.wexder.dbs2.domain.Merchandise;
import cz.wexder.dbs2.dto.MerchandiseFilterDto;
import cz.wexder.dbs2.dto.ResponseDto;
import cz.wexder.dbs2.service.merch.MerchandiseService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:8081")
@RestController("merchandise")
@RequestMapping("/merchandise")
public class MerchandiseController {

    private final MerchandiseService merchandiseService;

    public MerchandiseController(MerchandiseService merchandiseService) {
        this.merchandiseService = merchandiseService;
    }

    /**
     *
     * @return all merchandise
     */
    @GetMapping(value = "/all", produces = "application/json")
    public ResponseDto<List<Merchandise>> all() {
        return merchandiseService.getAll();
    }

    /**
     *
     * @param page number of page
     * @param size number
     * @return return paged merchandise
     */
    @GetMapping(value = "/all/page", produces = "application/json")
    public ResponseDto<List<Merchandise>> allPaged(@RequestParam("page") int page, @RequestParam("size") int size) {
        if (size < 1){
            size = Integer.MAX_VALUE;
        }
        return merchandiseService.getPage(page, size);
    }

    /**
     * Get filtered merchandise
     * @param filterDto to be filtered by
     * @return list of merchandise
     */
    @GetMapping(value = "/", produces = "application/json")
    public ResponseDto<List<Merchandise>> allFiltered(MerchandiseFilterDto filterDto) {
        return merchandiseService.getFiltered(filterDto);
    }

    /**
     * get one of merchandise by id
     * @param id to get by
     * @return return merchandise
     */
    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseDto<List<Merchandise>> getMerch(@PathVariable(value="id") Long id) {
        return merchandiseService.getOneById(id);
    }

    /**
     *
     * @return total count of merchandise
     */
    @GetMapping(value = "/count", produces = "application/json")
    public ResponseDto<Integer> getTotalCount() {
        return merchandiseService.getTotalCount();
    }

}
