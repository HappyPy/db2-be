package cz.wexder.dbs2.controller;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import cz.wexder.dbs2.UsernameAlreadyInUseException;
import cz.wexder.dbs2.domain.Login;
import cz.wexder.dbs2.dto.ErrorDto;
import cz.wexder.dbs2.dto.ResponseDto;
import cz.wexder.dbs2.service.user.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;

import static cz.wexder.dbs2.constants.SecurityConstants.HEADER_STRING;
import static cz.wexder.dbs2.constants.SecurityConstants.SECRET;

@CrossOrigin(origins = "http://localhost:8081")
@RestController("users")
@RequestMapping("/users")
public class UserController {
    final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * create new user
     *
     * @param user to be created
     * @return Success string or error
     */
    @PostMapping("/signUp")
    public ResponseDto<String> signUp(@RequestBody Login user) {
        try {
            userService.signUp(user);
            return new ResponseDto<>("Success", null);
        } catch (UsernameAlreadyInUseException e) {
            return new ResponseDto<>(null,
                    Collections.singletonList(new ErrorDto("Uživatelské jméno je již používané")));
        }
    }

    /**
     * get user info from JWT
     *
     * @param request
     * @param response
     * @return user info
     */
    @GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseDto<Login> getUserInfo(HttpServletRequest request, HttpServletResponse response) {
        String token = request.getHeader(HEADER_STRING);
        if (token != null) {
            String tokenUser = JWT.require(Algorithm.HMAC512(SECRET.getBytes()))
                    .build()
                    .verify(token).getSubject();
            ResponseDto<Login> responseDto = new ResponseDto<>(userService.getUserInfo(tokenUser));
            return responseDto;
        } else {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return new ResponseDto<>(null, Collections.singletonList(new ErrorDto("Missing token")));
        }
    }
}
