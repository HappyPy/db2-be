package cz.wexder.dbs2.controller;

import cz.wexder.dbs2.domain.Merchandise;
import cz.wexder.dbs2.domain.Order;
import cz.wexder.dbs2.dto.AdminOrderDto;
import cz.wexder.dbs2.dto.ResponseDto;
import cz.wexder.dbs2.service.merch.MerchandiseService;
import cz.wexder.dbs2.service.orders.OrdersService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:8081")
@RestController("admin")
@RequestMapping("/admin")
public class AdminController {

    private final MerchandiseService merchandiseService;
    private final OrdersService ordersService;

    public AdminController(MerchandiseService merchandiseService, OrdersService ordersService) {
        this.merchandiseService = merchandiseService;
        this.ordersService = ordersService;
    }

    /**
     * Saves newly created merchandise or edited one
     * @param merchandise to be saved
     * @return success string
     */
    @PutMapping(value = "/merchandise", produces = "application/json")
    public ResponseDto<String> saveMerchandise(@RequestBody Merchandise merchandise) {
        return merchandiseService.saveMerchandise(merchandise);
    }

    /**
     * Deletes given merchandise
     * @param id to be deleted
     * @return @return success bool
     */
    @DeleteMapping(value = "/merchandise/{id}", produces = "application/json")
    public ResponseDto<Boolean> deleteMerchandise(@PathVariable Long id) {
        return merchandiseService.deleteMerchandise(id);
    }

    /**
     * Get all orders for admin
     * @return List of orders wrapped in response dto
     */
    @GetMapping(value = "/orders", produces = "application/json")
    public ResponseDto<List<AdminOrderDto>> getAllOrders() {
        return new ResponseDto<>(ordersService.getall());
    }
}
