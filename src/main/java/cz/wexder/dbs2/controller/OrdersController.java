package cz.wexder.dbs2.controller;


import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import cz.wexder.dbs2.domain.Order;
import cz.wexder.dbs2.dto.ErrorDto;
import cz.wexder.dbs2.dto.ResponseDto;
import cz.wexder.dbs2.service.orders.OrdersService;
import cz.wexder.dbs2.service.user.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Base64;
import java.util.Collections;
import java.util.List;

import static cz.wexder.dbs2.constants.SecurityConstants.HEADER_STRING;
import static cz.wexder.dbs2.constants.SecurityConstants.SECRET;

@CrossOrigin(origins = "http://localhost:8081")
@RestController("orders")
@RequestMapping("/orders")
public class OrdersController {
    final OrdersService ordersService;
    final UserService userService;

    public OrdersController(OrdersService ordersService, UserService userService) {
        this.ordersService = ordersService;
        this.userService = userService;
    }

    /**
     * get order for given user
     *
     * @param request
     * @param response
     * @return list of orders
     */
    @GetMapping("/")
    public ResponseDto<List<Order>> getOrderForUser(HttpServletRequest request, HttpServletResponse response) {
        String token = request.getHeader(HEADER_STRING);
        if (token != null) {
            String tokenUser = JWT.require(Algorithm.HMAC512(Base64.getDecoder().decode(SECRET)))
                    .build()
                    .verify(token).getSubject();
            Long idByLogin = userService.getUserIdByLogin(tokenUser);
            return new ResponseDto<>(ordersService.getAllUserOrders(idByLogin));
        } else {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return new ResponseDto<>(null, Collections.singletonList(new ErrorDto("Missing token")));
        }
    }

    /**
     * create new order
     *
     * @param order to be created
     * @return created order
     */
    @PostMapping("/create")
    public ResponseDto<Order> createOrder(@RequestBody Order order) {
        return new ResponseDto<>(ordersService.createOrder(order));
    }
}
