package cz.wexder.dbs2.dto;

import cz.wexder.dbs2.domain.Order;
import cz.wexder.dbs2.domain.User;
import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
public @Data
class AdminOrderDto {
    private Order order;
    private User user;
}
