package cz.wexder.dbs2.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

public @Data @AllArgsConstructor
class ErrorDto {
    private String message;
}
