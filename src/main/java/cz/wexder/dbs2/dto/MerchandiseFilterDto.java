package cz.wexder.dbs2.dto;

import lombok.Data;

import java.util.List;

public @Data class MerchandiseFilterDto {
    private int page;
    private int size;
    private int priceMin;
    private int priceMax;
    private List<String> categories;
}
