package cz.wexder.dbs2.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

public @Data
@AllArgsConstructor
class ResponseDto<T> {
    T data;
    List<ErrorDto> errors;

    public ResponseDto(T data) {
        this.data = data;
    }

}
