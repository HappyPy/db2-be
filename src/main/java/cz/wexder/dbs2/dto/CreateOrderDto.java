package cz.wexder.dbs2.dto;

import lombok.Data;

public @Data
class CreateOrderDto {
    Long merchandiseId;
    Long amount;
}
