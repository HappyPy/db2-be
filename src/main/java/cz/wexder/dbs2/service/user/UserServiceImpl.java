package cz.wexder.dbs2.service.user;

import cz.wexder.dbs2.UsernameAlreadyInUseException;
import cz.wexder.dbs2.domain.Login;
import cz.wexder.dbs2.domain.LoginType;
import cz.wexder.dbs2.domain.User;
import cz.wexder.dbs2.repository.LoginRepository;
import cz.wexder.dbs2.repository.UserRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class UserServiceImpl implements UserService {

    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final LoginRepository loginRepository;
    private final UserRepository userRepository;

    public UserServiceImpl(BCryptPasswordEncoder bCryptPasswordEncoder, LoginRepository loginRepository, UserRepository userRepository) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.loginRepository = loginRepository;
        this.userRepository = userRepository;
    }

    /**
     * Get user id by given username
     * @param login username
     * @return return id of user
     */
    @Override
    public Long getUserIdByLogin(String login){
        return loginRepository.findByUsername(login).getId();
    }

    /**
     * Get user info for given username
     * @param login username
     * @return user info
     */
    @Override
    @Transactional
    public Login getUserInfo(String login) {
        return loginRepository.findByUsername(login);
    }

    /**
     * Sign up new user
     * @param user user to be created
     * @throws UsernameAlreadyInUseException
     */
    @Override
    @Transactional
    public void signUp(Login user) throws UsernameAlreadyInUseException {
        Login repositoryUser = loginRepository.findByUsername(user.getUsername());
        if(repositoryUser == null){
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            user.setLoginType(LoginType.UZIVATEL);
            loginRepository.save(user);
        }else{
            throw new UsernameAlreadyInUseException(user.getUsername());
        }
    }

    @Override
    public User createUser(User user) {
        return userRepository.save(user);
    }
}
