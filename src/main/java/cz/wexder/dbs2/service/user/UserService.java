package cz.wexder.dbs2.service.user;

import cz.wexder.dbs2.UsernameAlreadyInUseException;
import cz.wexder.dbs2.domain.Login;
import cz.wexder.dbs2.domain.User;

public interface UserService {

    Long getUserIdByLogin(String login);
    Login getUserInfo(String login);
    void signUp(Login user) throws UsernameAlreadyInUseException;
    User createUser(User user);
}
