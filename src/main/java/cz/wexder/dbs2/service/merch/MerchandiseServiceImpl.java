package cz.wexder.dbs2.service.merch;

import cz.wexder.dbs2.domain.Category;
import cz.wexder.dbs2.domain.Merchandise;
import cz.wexder.dbs2.domain.MerchandiseCategory;
import cz.wexder.dbs2.dto.MerchandiseFilterDto;
import cz.wexder.dbs2.dto.ResponseDto;
import cz.wexder.dbs2.repository.CategoriesRepository;
import cz.wexder.dbs2.repository.MerchandiseCategoryRepository;
import cz.wexder.dbs2.repository.MerchandisePhotoRepository;
import cz.wexder.dbs2.repository.MerchandiseRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MerchandiseServiceImpl implements MerchandiseService {
    private final MerchandiseRepository merchandiseRepository;
    private final CategoriesRepository categoriesRepository;
    private final MerchandiseCategoryRepository merchandiseCategoryRepository;
    private final MerchandisePhotoRepository merchandisePhotoRepository;

    public MerchandiseServiceImpl(MerchandiseRepository merchandiseRepository, CategoriesRepository categoriesRepository, MerchandiseCategoryRepository merchandiseCategoryRepository, MerchandisePhotoRepository merchandisePhotoRepository) {
        this.merchandiseRepository = merchandiseRepository;
        this.categoriesRepository = categoriesRepository;
        this.merchandiseCategoryRepository = merchandiseCategoryRepository;
        this.merchandisePhotoRepository = merchandisePhotoRepository;
    }

    /**
     *
     * @return return all merchandise
     */
    @Override
    public ResponseDto<List<Merchandise>> getAll() {
        List<Merchandise> all = merchandiseRepository.findAll();
        return new ResponseDto<>(all);
    }

    /**
     * Get one merch by id
     * @param id to get
     * @return return singleton list of merch
     */
    @Override
    @Transactional
    public ResponseDto<List<Merchandise>> getOneById(Long id) {
        Merchandise one = merchandiseRepository.getOne(id);
        return new ResponseDto<>(Collections.singletonList(one));
    }

    /**
     * Get one page of merchandise
     * @param page number of page
     * @param size number
     * @return page of merchandise
     */
    @Override
    public ResponseDto<List<Merchandise>> getPage(int page, int size) {
        Page<Merchandise> all = merchandiseRepository.findAll(PageRequest.of(page, size));
        return new ResponseDto<>(all.toList());
    }

    /**
     * Get list of merchandise by filter
     * @param filterDto filter to get merchandise by
     * @return return list of merchandise
     */
    @Override
    public ResponseDto<List<Merchandise>> getFiltered(MerchandiseFilterDto filterDto) {
        if (filterDto.getPriceMax() == 0){
            filterDto.setPriceMax(Integer.MAX_VALUE);
        }
        if(filterDto.getCategories() == null || filterDto.getCategories().isEmpty()){
            List<String> allCategories = categoriesRepository.findAll().stream().map(Category::getName).collect(Collectors.toList());
            filterDto.setCategories(allCategories);
        }
        Page<Merchandise> all =
                merchandiseRepository.findAllByPricePerOnceBetweenAndCategories(
                        filterDto.getPriceMin(),
                        filterDto.getPriceMax(),
                        filterDto.getCategories() != null && filterDto.getCategories().isEmpty() ? Collections.emptyList() : filterDto.getCategories(),
                        PageRequest.of(filterDto.getPage(),
                                filterDto.getSize()));
        return new ResponseDto<>(all.toList());
    }


    /**
     *
     * @return count of all merchandise
     */
    @Override
    public ResponseDto<Integer> getTotalCount() {
        return new ResponseDto<>(merchandiseRepository.getTotalCount());
    }

    /**
     * save edited or create merchandise
     * @param merchandise to be save
     * @return Success string or error
     */
    @Override
    @Transactional
    public ResponseDto<String> saveMerchandise(Merchandise merchandise) {
        List<MerchandiseCategory> categories = merchandise.getCategories();
        if (merchandise.getId() == null) {
            merchandise.setCategories(null);
            merchandiseRepository.save(merchandise);
        }
        Merchandise repositoryOne = merchandiseRepository.getOne(merchandise.getId());
        merchandiseCategoryRepository.removeAllByMerchandiseId(repositoryOne.getId());
        LinkedList<MerchandiseCategory> mappedCategories = new LinkedList<>();
        for (MerchandiseCategory merchandiseCategory : categories) {
            Category one = categoriesRepository.getOne(merchandiseCategory.getCategory().getId());
            merchandiseCategory.setCategory(one);
            merchandiseCategory.setMerchandise(repositoryOne);
            mappedCategories.add(merchandiseCategoryRepository.save(merchandiseCategory));
        }
        if (!repositoryOne.getPhotos().equals(merchandise.getPhotos())) {
            merchandise.getPhotos().forEach(merchandisePhoto -> {
                if (!repositoryOne.getPhotos().contains(merchandisePhoto)) {
                    merchandisePhoto.setMerchandise(repositoryOne);
                    merchandisePhotoRepository.save(merchandisePhoto);
                }
            });
            if (merchandise.getPhotos().size() < repositoryOne.getPhotos().size()) {
                repositoryOne.getPhotos().forEach(merchandisePhoto -> {
                    if (!merchandise.getPhotos().contains(merchandisePhoto)) {
                        merchandisePhotoRepository.deleteById(merchandisePhoto.getId());
                    }
                });
            }
        }
        merchandise.setCategories(mappedCategories);
        merchandiseRepository.save(merchandise);
        return new ResponseDto<>("Success");
    }

    /**
     * delete merchandise by id
     * @param id to be deleted
     * @return bool if Success
     */
    @Override
    public ResponseDto<Boolean> deleteMerchandise(Long id) {
        merchandiseRepository.deleteById(id);
        return new ResponseDto<>(Boolean.TRUE);
    }
}
