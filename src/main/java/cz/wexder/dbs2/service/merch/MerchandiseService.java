package cz.wexder.dbs2.service.merch;

import cz.wexder.dbs2.domain.Merchandise;
import cz.wexder.dbs2.dto.MerchandiseFilterDto;
import cz.wexder.dbs2.dto.ResponseDto;

import java.util.List;

public interface MerchandiseService {
    ResponseDto<List<Merchandise>> getAll();
    ResponseDto<List<Merchandise>> getOneById(Long id);
    ResponseDto<List<Merchandise>> getPage(int page, int size);
    ResponseDto<List<Merchandise>> getFiltered(MerchandiseFilterDto filterDto);

    ResponseDto<Integer> getTotalCount();

    ResponseDto<String> saveMerchandise(Merchandise merchandise);
    ResponseDto<Boolean> deleteMerchandise(Long id);
}
