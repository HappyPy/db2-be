package cz.wexder.dbs2.service.orders;

import cz.wexder.dbs2.domain.Order;
import cz.wexder.dbs2.dto.AdminOrderDto;

import java.util.List;

public interface OrdersService {

    List<Order> getAllUserOrders(Long userId);

    Order createOrder(Order order);

    List<AdminOrderDto> getall();
}
