package cz.wexder.dbs2.service.orders;

import cz.wexder.dbs2.domain.*;
import cz.wexder.dbs2.dto.AdminOrderDto;
import cz.wexder.dbs2.repository.OrdersRepository;
import cz.wexder.dbs2.service.user.UserService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrdersServiceImpl implements OrdersService {

    private OrdersRepository ordersRepository;
    private UserService userService;

    public OrdersServiceImpl(OrdersRepository ordersRepository, UserService userService) {
        this.ordersRepository = ordersRepository;
        this.userService = userService;
    }

    /**
     * Get all orders for given user
     * @param userId
     * @return List of orders
     */
    @Override
    public List<Order> getAllUserOrders(Long userId) {
        User user = new User();
        user.setId(userId);
        return ordersRepository.findAllByUser(user);
    }

    /**
     * Creates order
     * @param order dto to create order
     * @return created order
     */
    @Override
    public Order createOrder(Order order) {
        if(order.getUser().getId() == null){
            Login userInfo = userService.getUserInfo(order.getUser().getName());
            if(userInfo == null){
                order.setUser(userService.createUser(order.getUser()));
            }else{
                order.setUser(userInfo.getUser());
            }
        }
        order.setCreated(new Date());
        List<OrderMerchandise> merchandises = order.getMerchandises();
        order.setMerchandises(null);
        Order save = ordersRepository.save(order);
        merchandises.forEach(orderMerchandise -> orderMerchandise.setOrder(order));
        order.setMerchandises(merchandises);
        OrderStatus orderStatus = new OrderStatus();
        orderStatus.setStatus("ZALOZENO");
        orderStatus.setId(save.getId());
        save.setOrderStatus(orderStatus);
        ordersRepository.save(save);
        return save;
    }
    /**
     * Get all orders for admin
     * @return List of all orders
     */
    @Override
    public List<AdminOrderDto> getall() {
        List<Order> all = ordersRepository.findAll();
        return all.stream().map(order -> {
            User user = order.getUser();
            user.setOrders(null);
            return new AdminOrderDto(order, user);
        }).collect(Collectors.toList());
    }

}
