package cz.wexder.dbs2.repository;

import cz.wexder.dbs2.domain.Login;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginRepository extends JpaRepository<Login, Long> {

    Login findByUsername(String username);
}
