package cz.wexder.dbs2.repository;

import cz.wexder.dbs2.domain.Order;
import cz.wexder.dbs2.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrdersRepository extends JpaRepository<Order, Long> {

    List<Order> findAllByUser(User user);
}
