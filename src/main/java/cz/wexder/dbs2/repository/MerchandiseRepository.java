package cz.wexder.dbs2.repository;

import cz.wexder.dbs2.domain.Merchandise;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MerchandiseRepository extends JpaRepository<Merchandise, Long> {

    @Query(value = "select m from Merchandise m join m.categories c join c.category cat  where m.price between :priceMin and :priceMax and cat.name in :categories")
    Page<Merchandise> findAllByPricePerOnceBetweenAndCategories(@Param("priceMin") int priceMin, @Param("priceMax") int priceMax, @Param("categories") List<String> categories, Pageable pageable);

    @Query("select count (m.name) from Merchandise m")
    Integer getTotalCount();
}
