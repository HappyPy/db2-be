package cz.wexder.dbs2.repository;


import cz.wexder.dbs2.domain.MerchandisePhoto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MerchandisePhotoRepository extends JpaRepository<MerchandisePhoto, Long> {

}
