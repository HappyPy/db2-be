package cz.wexder.dbs2.repository;


import cz.wexder.dbs2.domain.MerchandiseCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MerchandiseCategoryRepository extends JpaRepository<MerchandiseCategory, Long> {

    @Modifying
    @Query("delete from MerchandiseCategory m where m.merchandise.id = :merchandiseId")
    void removeAllByMerchandiseId(Long merchandiseId);
}
