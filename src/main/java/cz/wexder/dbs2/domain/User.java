package cz.wexder.dbs2.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "UZIVATEL")
@SequenceGenerator(name = "SEQ_UZIVATEL", sequenceName = "SEQ_UZIVATEL", allocationSize = 1)
public @Data
class User {

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_UZIVATEL")
    @Column(name = "UZIVATEL_ID")
    private Long Id;

    @Column(name = "JMENO")
    private String name;

    @Column(name = "PRIJMENI")
    private String surname;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "TELEFON")
    private String phone;

    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "ADRESA_ID")
    private Address address;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Order> orders;

}
