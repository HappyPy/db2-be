package cz.wexder.dbs2.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "PRIHLASENI")
@SequenceGenerator(name = "SEQ_PRIHLASENI", sequenceName = "SEQ_PRIHLASENI", allocationSize = 1)
public @Data
class Login {

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PRIHLASENI")
    @Column(name = "PRIHLASENI_ID")
    private Long Id;

    @Column(name = "JMENO")
    private String username;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name = "HESLO")
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(name = "TYP")
    private LoginType loginType;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "UZIVATEL_ID")
    private User user;
}
