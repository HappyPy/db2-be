package cz.wexder.dbs2.domain;

public enum PaymentType {
    ONLINE, TRANSFER
}
