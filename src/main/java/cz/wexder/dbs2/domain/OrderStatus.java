package cz.wexder.dbs2.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "STAV_OBJEDNAVKY")
public @Data class OrderStatus {

    @Id
    @Column(name = "STAV_OBJEDNAVKY_ID")
    private Long Id;

    @Column(name = "STAV")
    private String status;
}
