package cz.wexder.dbs2.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "PLATBA")
@SequenceGenerator(name = "SEQ_PLATBA", sequenceName = "SEQ_PLATBA", allocationSize = 1)
public @Data
class Payment {

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PLATBA")
    @Column(name = "PLATBA_ID")
    private Long Id;

    @Column(name = "PLATBA_TYP")
    private String paymentType;

    @Column(name = "ZAPLACENO")
    private Date payed;
}
