package cz.wexder.dbs2.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "KATEGORIE_ZBOZI")
@SequenceGenerator(name = "SEQ_KATEGORIE_ZBOZI", sequenceName = "SEQ_KATEGORIE_ZBOZI", allocationSize = 1)
@JsonIgnoreProperties(value = {"merchandise", "hibernateLazyInitializer", "handler"})
public @Data
class MerchandiseCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_KATEGORIE_ZBOZI")
    @Column(name = "KAT_ZBOZI_ID")
    private Long Id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "KATEGORIE_ID", nullable = false)
    private Category category;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ZBOZI_ID")
    private Merchandise merchandise;
}