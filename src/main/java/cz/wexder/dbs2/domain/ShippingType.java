package cz.wexder.dbs2.domain;

public enum ShippingType {
    OSOBNE, POSTA, ZASILKOVNA
}
