package cz.wexder.dbs2.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "ADRESA")
@SequenceGenerator(name = "SEQ_ADRESA", sequenceName = "SEQ_ADRESA", allocationSize = 1)
public @Data
class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ADRESA")
    @Column(name = "ADRESA_ID")
    private Long Id;

    @Column(name = "MESTO")
    private String city;

    @Column(name = "ULICE")
    private String street;

    @Column(name = "CISLO_POPISNE")
    private String houseNumber;

    @Column(name = "PSC")
    private int postCode;
}
