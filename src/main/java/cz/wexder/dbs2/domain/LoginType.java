package cz.wexder.dbs2.domain;

import org.springframework.security.core.GrantedAuthority;

public enum LoginType implements GrantedAuthority {
    UZIVATEL("UZIVATEL"), ADMINISTRATOR("ADMINISTRATOR"), MARKETING("MARKETING");

    private final String role;

    LoginType(String role) {
        this.role = role;
    }

    @Override
    public String getAuthority() {
        return role;
    }
}
