package cz.wexder.dbs2.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "ZBOZI")
@SequenceGenerator(name = "SEQ_ZBOZI", sequenceName = "SEQ_ZBOZI", allocationSize = 1)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public @Data
class Merchandise {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ZBOZI")
    @Column(name = "ZBOZI_ID")
    private Long id;

    @Column(name = "NAZEV")
    private String name;

    @Column(name = "CENA_ZA_KUS")
    private int price;

    @Column(name = "POCET_NA_SKLADE")
    private int availableInWarehouse;

    @Column(name = "POCET_CELKEM")
    private int availableAll;

    @Column(name = "POPIS")
    private String description;

    @Column(name = "KRATKY_POPIS")
    private String shortDescription;

    @OneToMany(mappedBy = "merchandise", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<MerchandisePhoto> photos;

    @OneToMany(mappedBy = "merchandise", cascade = CascadeType.ALL)
    private List<MerchandiseCategory> categories;
}
