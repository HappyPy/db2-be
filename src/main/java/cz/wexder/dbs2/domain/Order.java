package cz.wexder.dbs2.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "OBJEDNAVKA")
@SequenceGenerator(name = "SEQ_OBJEDNAVKA", sequenceName = "SEQ_OBJEDNAVKA", allocationSize = 1)
public @Data
class Order {

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_OBJEDNAVKA")
    @Column(name = "OBJEDNAVKA_ID")
    private Long Id;

    @Column(name = "VYTVORENA")
    private Date created;

    @Column(name = "CENA")
    private Long price;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "PLATBA_ID")
    private Payment payment;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "STAV_OBJEDNAVKY_ID")
    private OrderStatus orderStatus;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "UZIVATEL_ID")
    private User user;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "DOD_ADRESA")
    private Address transportAddress;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "DOPRAVA_ID")
    private Transport transport;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "OBJEDNAVKA_ID")
    private List<OrderMerchandise> merchandises;
}
