package cz.wexder.dbs2.domain;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "DOPRAVA")
@SequenceGenerator(name = "SEQ_DOPRAVA", sequenceName = "SEQ_DOPRAVA", allocationSize = 1)
public @Data
class Transport {

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DOPRAVA")
    @Column(name = "DOPRAVA_ID")
    private Long Id;

    @Column(name = "DOPRAVA_TYP")
    private String transportType;

}
