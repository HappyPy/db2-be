package cz.wexder.dbs2.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "KATEGORIE")
@SequenceGenerator(name = "SEQ_KATEGORIE", sequenceName = "SEQ_KATEGORIE", allocationSize = 1)
public @Data
class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_KATEGORIE")
    @Column(name = "KATEGORIE_ID")
    private Long Id;

    @Column(name = "NAZEV")
    private String name;

    @Column(name = "POPIS")
    private String categoryDescription;
}
