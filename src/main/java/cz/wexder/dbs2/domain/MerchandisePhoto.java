package cz.wexder.dbs2.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "ZBOZI_PHOTO")
@SequenceGenerator(name = "SEQ_ZBOZI_PHOTO", sequenceName = "SEQ_ZBOZI_PHOTO", allocationSize = 1)
@JsonIgnoreProperties(value = { "merchandise", "hibernateLazyInitializer", "handler"})
public @Data
class MerchandisePhoto {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ZBOZI_PHOTO")
    @Column(name = "ZBOZI_PHOTO_ID")
    private Long Id;

    @Lob
    @Column(name = "PHOTO")
    private String photo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="ZBOZI_ID", nullable=false)
    private Merchandise merchandise;
}
