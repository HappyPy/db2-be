package cz.wexder.dbs2.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "OBJEDNAVKA_ZBOZI")
@SequenceGenerator(name = "SEQ_OBJEDNAVKA_ZBOZI", sequenceName = "SEQ_OBJEDNAVKA_ZBOZI", allocationSize = 1)
@JsonIgnoreProperties(value = { "merchandise", "hibernateLazyInitializer", "handler"})
public @Data class OrderMerchandise {

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_OBJEDNAVKA")
    @Column(name = "OBJ_ZBOZI_ID")
    private Long Id;

    @Column(name = "MNOZSTVI")
    private Long count;

    @Column(name = "CENA")
    private Long price;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OBJEDNAVKA_ID")
    private Order order;

    @Column(name = "ZBOZI_ID")
    private Long merchId;
}
