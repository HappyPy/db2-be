package cz.wexder.dbs2;

import lombok.Getter;

public class UsernameAlreadyInUseException extends Exception {
    @Getter
    private String username;

    public UsernameAlreadyInUseException(String username) {
        this.username = username;
    }
}
